#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char ** argv) {
	pid_t pid;
	int status;
	if(pid = fork()){
		printf("Parent process started\n");
		if ((pid = wait(&status)) == -1)
		                             // Wait for child process.                                  
		   perror("wait error");
		else {                       // Check status.                
		   if (WIFSIGNALED(status) != 0)
		      printf("Child process ended because of signal %d\n",
		              WTERMSIG(status));
		   else if (WIFEXITED(status) != 0)
		      printf("Child process ended normally; status = %d\n",
		              WEXITSTATUS(status));
		   else
		      printf("Child process did not end normally.n");
        	}
        	printf("Parent process ended\n");
        	exit(EXIT_SUCCESS);
	
	}else {
		printf("Child process started\n");
		execl("/home/Ali.Belakehal/labs/lab6/stopper", 
		"stopper", "-s", (char*)0);
	}
	
}
