#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char ** argv) {
	pid_t parent_pid = getpid();	
	pid_t pid;
	
	if(pid = fork()){
		printf("Parent process started with pid = %d\n", parent_pid);	
		printf("Parent process ended (pid = %d)\n", parent_pid);
        	exit(EXIT_SUCCESS);
	
	}else {
		printf("Demon child process started with pid = %d\n", getpid());	
		execl("/home/Ali.Belakehal/labs/lab6/stopper", 
			"stopper", "-s", (char*)0);
		printf("Demon child process ended (pid = %d)\n", getpid());
		exit(EXIT_SUCCESS);
	}
	
}
