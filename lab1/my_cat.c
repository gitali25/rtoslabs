#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main()
{
	int res = 0;
	while(true) {
		res = fgetc(stdin);
		if(res == EOF) break;
		fputc(res, stdout);
	}
	return(0);
}
