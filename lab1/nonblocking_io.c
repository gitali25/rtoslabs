#include<stdio.h>
#include <termios.h>        
#include <unistd.h>   

static struct termios old_context, new_context;

void save_context(){
	tcgetattr( STDIN_FILENO, &old_context);
	new_context = old_context;
}

void set_funny_terminal_mode(){
	new_context.c_lflag &= ~(ICANON);          
    tcsetattr( STDIN_FILENO, TCSANOW, &new_context);

	int c; 
	while((c = fgetc(stdin))!= 'q') fputc(c, stdout);
	fputc('\n', stdout);
}

void reload_context(){
	tcsetattr( STDIN_FILENO, TCSANOW, &old_context);
}


int main(void) {   
      
	save_context();
	set_funny_terminal_mode();
	reload_context();

    return 0;
}
