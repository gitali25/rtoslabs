#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void* foo(void* main_c_address, void* main_str_address) {
	int a = 7;
	int b = 7;
	printf("foo(): 1\na = %d, at : %p\nb = %d, at : %p\n", a, &a, b, &b);
	printf("foo(): 2\nprevious value : %d\nfollowing value : %d\n", *(&a + 1), *(&a - 1));
	void* ptr = &a;
	short distance = main_c_address - ptr;//76 + remove args
	short str_distance = main_str_address - ptr;//80 
	printf("foo(): 3\naddress of c in main : %p\n", (ptr + distance));
	printf("foo(): 4\nThe string in main : %s\n", ((char *)ptr + str_distance));
	return &str_distance + 2;
}

int main(void) {
	int a, b, c;
	char str[] = "I am a string muhahahahahahahahahaha!";
	printf("main():\na = %d, at : %p\nb = %d, at : %p\nc = %d, at : %p\n%s, at : %p, size : %lu\n\n", a, &a, b, &b, c, &c, str, str, sizeof(str));
	char* ptr = foo(&c, str);

	printf("Taks 2:\n");
	
	for(; ;) printf("%08x\n", *ptr++);
	
	printf("\n");
	return 0;
}
