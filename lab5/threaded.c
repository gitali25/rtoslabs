#include <pthread.h>
#include <stdio.h>

pthread_mutex_t lock;

void* power(void* number){
	int* casted = (int*) number;
	*casted *= *casted;
	return NULL;
}

void* increment(void* number){
	pthread_mutex_lock(&lock);
	int* casted = (int*) number;
	*casted += 1;
	pthread_mutex_unlock(&lock);
	return NULL;
}

int main() {
	
	printf("Theaded : \n");
/*
	int number = 7;
	int second_number = 7;
	pthread_t power_thread;
	if(pthread_create(&power_thread, NULL, power, &number)){
		printf("New thread not created");
		return 1;
	}
	second_number *= second_number;
	
	if(pthread_join(power_thread, NULL)){
		printf("threads not joined\n");
		return 2;
	}

	printf("First Number : %d\nSecond Number : %d\n", number, second_number);
*/
/*
	int shared_variable = 0;
	int iterations = 100000;
		while(iterations--){
		pthread_t inc_thread;
		if(pthread_create(&inc_thread, NULL, increment, &shared_variable)){
			printf("New thread not created");
			return 1;
		}
		shared_variable++;
	
		if(pthread_join(inc_thread, NULL)){
			printf("threads not joined\n");
			return 2;
		}
	}
	printf("shared variable : %d\n", shared_variable);

*/
	if (pthread_mutex_init(&lock, NULL))
    	{
        	printf("\nmutex init failed\n");
        	return 1;
    	}

	int shared_variable = 0;
	int iterations = 100000;
		while(iterations--){
		pthread_t inc_thread;
		if(pthread_create(&inc_thread, NULL, increment, &shared_variable)){
			printf("New thread not created\n");
		} else pthread_detach(inc_thread);
		pthread_mutex_lock(&lock);
		shared_variable++;
		pthread_mutex_unlock(&lock);
		
		
	}
	//pthread_exit();
	printf("shared variable : %d\n", shared_variable);
	pthread_mutex_destroy(&lock);
	return 0;
}

