#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int signal_arrived = 0;

void signal_handler(int sig) {
	/*
	static int ign_sig_counter = 1;
	printf("Ctrl-C pressed %d times\n", ign_sig_counter);
	signal(sig, SIG_IGN);
	ign_sig_counter++;
	signal(SIGINT, signal_handler);
	*/
	signal(sig,SIG_IGN);
	signal_arrived = 1;
	signal(SIGINT,signal_handler);
}

int main(void) {

	signal(SIGINT, signal_handler);
	unsigned int seconds = 500000;
	for(;;){
		if(signal_arrived){
			printf("\n%d\n", signal_arrived);
			signal_arrived = 0;
		}
		else printf(".\n");
		usleep(seconds);
	}
	return 0;
}
