#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

void print_directory_content(char* arg){
	DIR *d;
    	struct dirent *dir;
	d = opendir(arg);
	if (d) {
		while ((dir = readdir(d)) != NULL)
			printf((dir->d_name[0]=='.')? "" :"%s\n", dir->d_name);
		closedir(d);
	}
}
 
int main(int argc, char **argv)
{
    if(argc != 2) {
	//char current_working_dir[1024];
	//getcwd(current_working_dir, sizeof(current_working_dir));
	print_directory_content(".");
	return 0;
    }
    struct stat file_stat;
    if(stat(argv[1],&file_stat) < 0){  
	printf("Could not get file info\n");
        return 1;
    }
 
    printf("File : %s\n",argv[1]);
    printf("File Size: %d bytes\n",file_stat.st_size);
    printf("Owner has Permissions to: ");
    printf( (file_stat.st_mode & S_IRUSR) ? "read, " : "");
    printf( (file_stat.st_mode & S_IWUSR) ? "write, " : "");
    printf( (file_stat.st_mode & S_IXUSR) ? "execute\n" : "");
    printf("\n");
    if((S_ISDIR(file_stat.st_mode))){
	int status;
	char *args[3];
   	args[0] = "/bin/ls";
	args[1] = argv[1];
    	args[2] = NULL;
	if(fork() == 0) execv(args[0],args);
	else wait( &status );

/*
	print_directory_content(argv[1]);
*/
    }
    printf("------------------\n\n\ns");
    return 0;
}

